#!/usr/bin/python
from fake_useragent import UserAgent
import random, requests, time

# Add your web server url or IP below
url = 'http://'

ua = UserAgent()
for x in range(0, 250):
	print "Generating random user agent string\n"
	rando_agent = ua.random
	print("Using this UA string %s" % rando_agent)
	rtime = random.randint(30,90)
	print ("Sleeping for %s seconds" % rtime)
	time.sleep(rtime)
	headers = {'User-Agent': rando_agent}
	response = requests.get(url, headers=headers)
	#print response.content
